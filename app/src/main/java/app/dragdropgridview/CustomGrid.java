package app.dragdropgridview;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by admin on 5/24/2016.
 */
public class CustomGrid extends ArrayAdapter<String> {
    private final ArrayList menu_name;
    private final Activity context;
    String mname = "";
    public CustomGrid(Activity context, ArrayList<String> menu_name) {
        super(context, R.layout.item_grid,menu_name);
        this.context = context;

        this.menu_name = menu_name;

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.item_grid, null, true);
        TextView textView = (TextView)rowView.findViewById(R.id.title);
        textView.setText(""+menu_name.get(position));

        return rowView;
    }
}
