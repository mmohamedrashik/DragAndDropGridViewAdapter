package app.dragdropgridview;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;

import java.util.List;


public class DynamicAdapters extends BaseDynamicGridAdapter {
    public DynamicAdapters(Context context, List<String> items, int columnCount) {
        super(context, items, columnCount);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView titleText;
        ImageView image;

        convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_grid, null);
        titleText = (TextView) convertView.findViewById(R.id.title);
        image = (ImageView) convertView.findViewById(R.id.image);

        titleText.setText(getItem(position).toString());
        image.setImageResource(R.drawable.ic_launcher);
        return convertView;
    }


}