package app.dragdropgridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import org.askerov.dynamicgrid.DynamicGridView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    DynamicGridView gridView;
    List<String> list_item = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = (DynamicGridView) findViewById(R.id.gridview);

        list_item = getList_item();

        gridView.setAdapter(new DynamicAdapter(this, list_item,
                getResources().getInteger(R.integer.column_count)));

        gridView.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
                Log.e("position", "drag started at position " + position);
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
                Log.d("drag position", String.format("drag item position changed from %d to %d", oldPosition, newPosition));
            }
        });
        gridView.setOnDropListener(new DynamicGridView.OnDropListener() {
            @Override
            public void onActionDrop() {
                gridView.stopEditMode();
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                gridView.startEditMode(position);
                return true;
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, parent.getAdapter().getItem(position).toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    public List<String> getList_item(){
        list_item.add("New Delhi");
        list_item.add("Maharashtra");
        list_item.add("West Bengal");
        list_item.add("Madhya Pradesh");
        list_item.add("Bihar");
        list_item.add("Uttar Pradesh");
        list_item.add("Punjab");
        list_item.add("Assam");
        list_item.add("Goa");
        list_item.add("Gujrat");
        list_item.add("Kerala");
        list_item.add("Rajasthan");

        return list_item;
    }

    @Override
    public void onBackPressed() {
        if (gridView.isEditMode()) {
            gridView.stopEditMode();
        } else {
            super.onBackPressed();
        }
    }
}
