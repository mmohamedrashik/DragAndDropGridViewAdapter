package app.dragdropgridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import org.askerov.dynamicgrid.DynamicGridView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MyDynamic extends AppCompatActivity {
    DynamicGridView gridView;
   List<String> name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dynamic);
        gridView = (DynamicGridView)findViewById(R.id.gridviews);
        name = new ArrayList<>();
        name.add("RAZ");
        name.add("TEST");
        name.add("NAMES");


        gridView.setAdapter(new DynamicAdapters(MyDynamic.this,name,3));
        gridView.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
                Log.e("position", "drag started at position " + position);
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
                Log.d("drag position", String.format("drag item position changed from %d to %d", oldPosition, newPosition));
                int temp = newPosition;
                String temps = name.get(temp);
                name.set(newPosition,name.get(oldPosition));
                name.set(oldPosition,temps);
                for (int i=0;i<name.size();i++)
                {
                    Log.d("POSITION "+i,name.get(i));
                }

               /* Log.d("POSITION 1",name.get(0));
                Log.d("POSITION 2",name.get(1));
                Log.d("POSITION 3",name.get(2));*/
            }
        });
        gridView.setOnDropListener(new DynamicGridView.OnDropListener() {
            @Override
            public void onActionDrop() {
                gridView.stopEditMode();
                gridView.setAdapter(new DynamicAdapters(MyDynamic.this,name,3));
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                gridView.startEditMode(position);
                return true;
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MyDynamic.this, parent.getAdapter().getItem(position).toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}
